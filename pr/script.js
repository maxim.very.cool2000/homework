// function name() {
//   rar(name, age, sex)
//   {
//     console.log(`  ${name}     ${age}   ${sex}`)
//   }
// }

// name.rar("maxim", 18, "man")
function example() {
  console.log("Hello, World!"); // Виводить "Hello, World!" у консоль
  // Немає return
}

let result = example(); // result буде містити undefined, оскільки функція example() не повертає нічого явно

console.log(result); // Виведе undefined
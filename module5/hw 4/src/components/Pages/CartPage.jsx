import React from 'react';
import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';

const CartPage = ({
  cart,
  onRemoveOneFromCart,
  onAddToCart,
  onAddToFavorites,
  favorites,
  onRemoveFromCart,
  totalClassName = '',
  cartClassName = '',
  productCardClassName = '',
  headerClassName = '',
  cartTotalClassName = '',
}) => {
  if (!cart) return null;

  const handleAddToCart = (product) => {
    onAddToCart(product);
  };

  const handleAddToFavorites = (product) => {
    onAddToFavorites(product);
  };

  const handleRemoveOneFromCart = (product) => {
    onRemoveOneFromCart(product);
  };

  const handleRemoveFromCart = (product) => {
    onRemoveFromCart(product);
  };

  const total = cart.reduce((acc, item) => acc + item.price * item.quantity, 0);

  return (
    <div className={`cart-page ${cartClassName}`}>
      <header className={headerClassName}>
        <h2>Your Cart</h2>
      </header>
      <ul className="cart-products-list">
        {cart.map((item) => (
          <li key={item.id} className={`cart-product-item ${productCardClassName}`}>
            <ProductCard
              product={item}
              onAddToCart={handleAddToCart}
              onAddToFavorites={handleAddToFavorites}
              favorites={favorites}
              isCartPage={true} 
              onRemoveOneFromCart={handleRemoveOneFromCart}
              onRemoveFromCart={handleRemoveFromCart}
            />
          </li>
        ))}
      </ul>
      <div className={`cart-total ${cartTotalClassName}`}>
        <h3>Total: ${total.toFixed(2)}</h3>
      </div>
    </div>
  );
};

CartPage.propTypes = {
  cart: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      imageUrl: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
      quantity: PropTypes.number.isRequired,
    })
  ).isRequired,
  onRemoveOneFromCart: PropTypes.func.isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    })
  ).isRequired,
  onRemoveFromCart: PropTypes.func.isRequired,
  totalClassName: PropTypes.string,
  cartClassName: PropTypes.string,
  productCardClassName: PropTypes.string,
  headerClassName: PropTypes.string,
  cartTotalClassName: PropTypes.string,
};

export default CartPage;

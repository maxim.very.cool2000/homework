import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const saveToLocalStorage = (state) => {
  localStorage.setItem('cart', JSON.stringify(state.cart));
  localStorage.setItem('favorites', JSON.stringify(state.favorites));
};


const loadFromLocalStorage = () => {
  const cart = JSON.parse(localStorage.getItem('cart')) || [];
  const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
  return { cart, favorites };
};

export const fetchProducts = createAsyncThunk('products/fetchProducts', async () => {
  const response = await fetch('/products.json');
  const data = await response.json();
  return data;
});

const { cart, favorites } = loadFromLocalStorage();

const productsSlice = createSlice({
  name: 'products',
  initialState: {
    items: [],
    cart: cart,
    favorites: favorites,
    status: 'idle',
    error: null,
  },
  reducers: {
    addToCart(state, action) {
      const product = action.payload;
      const existingProduct = state.cart.find(item => item.id === product.id);
      if (existingProduct) {
        existingProduct.quantity += 1;
      } else {
        state.cart.push({ ...product, quantity: 1 });
      }
      saveToLocalStorage(state); 
    },
    removeFromCart(state, action) {
      const productId = action.payload.id;
      state.cart = state.cart.filter((item) => item.id !== productId); 
      saveToLocalStorage(state); 
    },
    addToFavorites(state, action) {
      const product = action.payload;
      if (!state.favorites.some(fav => fav.id === product.id)) {
        state.favorites.push(product);
      }
      saveToLocalStorage(state); 
    },
    removeFromFavorites(state, action) {
      const productId = action.payload.id;
      state.favorites = state.favorites.filter((item) => item.id !== productId);
      saveToLocalStorage(state); 
    },
    removeOneFromCart(state, action) {
      const productId = action.payload.id;
      const existingProduct = state.cart.find(item => item.id === productId);
      if (existingProduct && existingProduct.quantity > 1) {
        existingProduct.quantity -= 1;
      } else {
        state.cart = state.cart.filter(item => item.id !== productId);
      }
      saveToLocalStorage(state);
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.items = action.payload;
      })
      .addCase(fetchProducts.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message;
      });
  },
});

export const { addToCart, removeFromCart, addToFavorites, removeFromFavorites, removeOneFromCart } = productsSlice.actions;
export default productsSlice.reducer;
import React, { useEffect, useState } from 'react';
import './styles/App.scss';
import ProductList from './components/ProductList';
import Modal from './components/Modal/Modal';
import ModalHeader from './components/Modal/ModalHeader';
import ModalBody from './components/Modal/ModalBody';
import ModalFooter from './components/Modal/ModalFooter';
import ModalClose from './components/Modal/ModalClose';
import Cart from '../public/icon-header/cart.svg'

const App = () => {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  useEffect(() => {
    fetch('/products.json')
      .then(response => response.json())
      .then(data => setProducts(data));
  }, []);

  const handleAddToCartClick = (product) => {
    setSelectedProduct(product);
    setModalOpen(true);
  };

  const handleAddToCartConfirm = () => {
    setCart([...cart, selectedProduct]);
    setModalOpen(false);
  };

  const handleAddToFavorites = (product) => {
    if (!favorites.includes(product)) {
      setFavorites([...favorites, product]);
    } else {
      setFavorites(favorites.filter(fav => fav.id !== product.id));
    }
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };

  return (
    <div className="app">
      <header>
        <h1>Euphoria Shop</h1>
        <div className="cart-favorites">
          <span>{cart.length} <img src={Cart} alt="cart" width={20} height={20} /></span>
          <span>{favorites.length} ☆</span>
        </div>
      </header>
      <ProductList
        products={products}
        onAddToCart={handleAddToCartClick}
        onAddToFavorites={handleAddToFavorites}
        favorites={favorites}
      />
      {isModalOpen && (
        <Modal onClose={handleCloseModal}>
          <ModalHeader>
            <h2>Confirm Add to Cart</h2>
            <ModalClose onClick={handleCloseModal} />
          </ModalHeader>
          <ModalBody>
            <p>Do you want to add {selectedProduct.name} to your cart?</p>
          </ModalBody>
          <ModalFooter
            firstText="Cancel"
            firstClick={handleCloseModal}
            secondaryText="Add to Cart"
            secondaryClick={handleAddToCartConfirm}
          />
        </Modal>
      )}
    </div>
  );
};

export default App;

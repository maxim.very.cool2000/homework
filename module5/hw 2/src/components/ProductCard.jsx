import React from 'react';
import PropTypes from 'prop-types';

const ProductCard = ({ product, onAddToCart, onAddToFavorites, favorites }) => {
  const isFavorite = favorites.some(fav => fav.id === product.id);

  return (
    <div className="product-card">
      <img src={product.imageURL} alt={product.name} className="product-image" />
      <div className="product-info">
        <h3>{product.name}</h3>
        <p>Price: ${product.price.toFixed(2)}</p>
        <p>Color: {product.color}</p>
      <div className="buttons-flex" >
        <button className="btn-primary" onClick={() => onAddToCart(product)}>Add to Cart</button>
        <button 
          className={`btn-secondary ${isFavorite ? 'favorite' : ''}`}
          onClick={() => onAddToFavorites(product)}
        >
          {isFavorite ? '★' : '☆'}
        </button>
      </div>
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageUrl: PropTypes.string.isRequired,
    sku: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired
    })
  ).isRequired,
};

export default ProductCard;

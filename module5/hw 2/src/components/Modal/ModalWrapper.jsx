import React from 'react';
import PropTypes from 'prop-types';

const ModalWrapper = ({ children }) => {
  return <div className="modal-wrapper">{children}</div>;
};

ModalWrapper.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ModalWrapper;

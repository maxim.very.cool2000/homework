import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ type, classNames, onClick, children }) => {
  return (
    <button type={type} className={classNames} onClick={onClick}>
      {children}
    </button>
  );
};

Button.propTypes = {
  type: PropTypes.string,
  classNames: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

Button.defaultProps = {
  type: 'button',
  classNames: '',
};

export default Button;
  
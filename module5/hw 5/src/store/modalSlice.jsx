import { createSlice } from '@reduxjs/toolkit';

const modalSlice = createSlice({
  name: 'modal',
  initialState: {
    isOpen: false,
    isCheckoutModalOpen: false,
  },
  reducers: {
    openModal(state) {
      state.isOpen = true;
    },
    closeModal(state) {
      state.isOpen = false;
    },
    openCheckoutModal(state) {
      state.isCheckoutModalOpen = true;
    },
    closeCheckoutModal(state) {
      state.isCheckoutModalOpen = false;
    },
  },
});

export const { openModal, closeModal, openCheckoutModal, closeCheckoutModal } = modalSlice.actions;
export default modalSlice.reducer;

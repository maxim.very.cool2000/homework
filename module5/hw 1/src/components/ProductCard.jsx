import React from 'react';
import PropTypes from 'prop-types';

const ProductCard = ({ product, onAddToCart, onAddToFavorites }) => {
  const { name, price, imageUrl, sku, color } = product;

  return (
    <div className="product-card">
      <img src={imageUrl} alt={name} className="product-image" />
      <div className="product-info">
        <h3>{name}</h3>
        <p>${price.toFixed(2)}</p>
        <p>SKU: {sku}</p>
        <p>Color: {color}</p>
        <button className="btn-primary" onClick={() => onAddToCart(product)}>Add to Cart</button>
        <button className="btn-secondary" onClick={() => onAddToFavorites(product)}>☆</button>
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageUrl: PropTypes.string.isRequired,
    sku: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
};

export default ProductCard;
import React from 'react';
import PropTypes from 'prop-types';
import ModalWrapper from './ModalWrapper';
import ModalHeader from './ModalHeader';
import ModalFooter from './ModalFooter';
import ModalClose from './ModalClose';
import ModalBody from './ModalBody';

const Modal = ({ children, onClose }) => {
  return (
    <ModalWrapper onClick={onClose}>
      <div className="modal" onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </ModalWrapper>
  );
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default Modal;
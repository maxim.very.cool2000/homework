import React from 'react';
import PropTypes from 'prop-types';

const ModalFooter = ({ 
  firstText, 
  secondaryText, 
  firstClick, 
  secondaryClick,
  firstClass, 
  secondClass 
}) => {
  return (
    <div className="modal-footer">
      {firstText && <button className={firstClass} onClick={firstClick}>{firstText}</button>}
      {secondaryText && <button className={secondClass} onClick={secondaryClick}>{secondaryText}</button>}
    </div>
  );
};

ModalFooter.propTypes = {
  firstText: PropTypes.string,
  secondaryText: PropTypes.string,
  firstClick: PropTypes.func,
  secondaryClick: PropTypes.func,
  firstClass: PropTypes.string,
  secondClass: PropTypes.string,
};

export default ModalFooter;
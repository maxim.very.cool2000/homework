import React from 'react';
import PropTypes from 'prop-types';

const ModalWrapper = ({ children, onClick }) => {
  return (
    <div className="modal-wrapper" onClick={onClick}>
      {children}
    </div>
  );
};

ModalWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ModalWrapper;
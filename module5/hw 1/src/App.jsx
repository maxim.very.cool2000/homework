import React, { useState, useEffect } from 'react';
import './styles/App.scss';
import Button from './components/Button';
import Modal from './components/Modal/Modal';
import ModalHeader from './components/Modal/ModalHeader';
import ModalBody from './components/Modal/ModalBody';
import ModalFooter from './components/Modal/ModalFooter';
import ModalClose from './components/Modal/ModalClose';
import ProductList from './components/ProductList';

const App = () => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [modalProduct, setModalProduct] = useState(null);
  const [cart, setCart] = useState(() => JSON.parse(localStorage.getItem('cart')) || []);
  const [favorites, setFavorites] = useState(() => JSON.parse(localStorage.getItem('favorites')) || []);
  const products = [
    {
      id: 1,
      name: 'Black Sweatshirt',
      price: 123.00,
      imageUrl: 'https://example.com/black-sweatshirt.jpg',
      sku: 'SKU001',
      color: 'Black'
    },
    {
      id: 2,
      name: 'Line Pattern Black Hoodie',
      price: 37.00,
      imageUrl: 'https://example.com/line-pattern-hoodie.jpg',
      sku: 'SKU002',
      color: 'Black'
    },
    // Додайте ще товари
  ];

  const handleAddToCart = (product) => {
    setCart(prevCart => {
      const updatedCart = [...prevCart, product];
      localStorage.setItem('cart', JSON.stringify(updatedCart));
      return updatedCart;
    });
    setModalProduct(product);
    setModalOpen(true);
  };

  const handleAddToFavorites = (product) => {
    setFavorites(prevFavorites => {
      const updatedFavorites = prevFavorites.some(item => item.id === product.id)
        ? prevFavorites.filter(item => item.id !== product.id)
        : [...prevFavorites, product];
      localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
      return updatedFavorites;
    });
  };

  return (
    <div className="app">
      <header>
        <h1>Euphoria</h1>
        <div className="icons">
          <div className="cart-icon">
            <span role="img" aria-label="cart">🛒</span>
            <span>{cart.length}</span>
          </div>
          <div className="favorites-icon">
            <span role="img" aria-label="favorites">⭐</span>
            <span>{favorites.length}</span>
          </div>
        </div>
      </header>
      <ProductList
        products={products}
        onAddToCart={handleAddToCart}
        onAddToFavorites={handleAddToFavorites}
      />
      {isModalOpen && modalProduct && (
        <Modal onClose={() => setModalOpen(false)}>
          <ModalHeader>
            <h2>{modalProduct.name}</h2>
            <ModalClose onClick={() => setModalOpen(false)} />
          </ModalHeader>
          <ModalBody>
            <p>Product {modalProduct.name} has been added to the cart.</p>
          </ModalBody>
          <ModalFooter
            firstText="Close"
            firstClick={() => setModalOpen(false)}
            firstClass="btn-primary"
          />
        </Modal>
      )}
    </div>
  );
};

export default App;

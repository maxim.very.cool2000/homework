import React from 'react';
import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';

const FavoritesPage = ({ favorites, onAddToCart, onAddToFavorites }) => {
  return (
    <div className="product-list">
      {favorites.map(product => (
        <ProductCard
          key={product.id}
          product={product}
          onAddToCart={onAddToCart}
          onAddToFavorites={onAddToFavorites}
          favorites={favorites}
        />
      ))}
    </div>
  );
};

FavoritesPage.propTypes = {
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      imageUrl: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
    })
  ).isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
};

export default FavoritesPage;

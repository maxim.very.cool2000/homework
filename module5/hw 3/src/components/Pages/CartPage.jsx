import React from 'react';
import PropTypes from 'prop-types';

const CartPage = ({ cart, onRemoveFromCart, onAddToCart, onAddToFavorites, favorites }) => {
  const totalSum = cart.reduce((sum, product) => sum + product.price, 0);

  return (
    <div className="cart-page">
      <h2>Cart</h2>
      <div className="cart-list">
        {cart.map((product, index) => (
          <div key={index} className="cart-item">
            <img src={product.imageUrl} alt={product.name} className="cart-item-image" />
            <div className="cart-item-info">
              <h3>{product.name}</h3>
              <p>Price: ${product.price.toFixed(2)}</p>
              <p>Color: {product.color}</p>
            </div>
            <button className="btn-remove" onClick={() => onRemoveFromCart(product)}>delete</button>
          </div>
        ))}
      </div>
      <div className="cart-total">
        <h3>Total price: ${totalSum.toFixed(2)}</h3>
      </div>
    </div>
  );
};

CartPage.propTypes = {
  cart: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      imageUrl: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
    })
  ).isRequired,
  onRemoveFromCart: PropTypes.func.isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    })
  ).isRequired,
};

export default CartPage;

import React from 'react';
import PropTypes from 'prop-types';
import '../../styles/Modal.scss';

const Modal = ({ onClose, children }) => {
  const handleWrapperClick = (e) => {
    if (e.target === e.currentTarget) {
      onClose();
    }
  };

  return (
    <div className="modal-wrapper" onClick={handleWrapperClick}>
      <div className="modal">
        {children}
      </div>
    </div>
  );
};

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default Modal;

import React from 'react';
import PropTypes from 'prop-types';

const ProductCard = ({ product, onAddToCart, onAddToFavorites, favorites = [], isCartPage, isFavoritesPage }) => {
  if (!product) return null;

  const { id, name, price, imageUrl, color } = product;

  const isFavorite = favorites ? favorites.some(fav => fav.id === id) : false;

  return (
    <div className="product-card">
      <img src={imageUrl} alt={name} className="product-image" />
      <div className="product-info">
        <h3>{name}</h3>
        <p>Price: ${price.toFixed(2)}</p>
        <p>Color: {color}</p>
        <div className="buttons-flex">
          <button className="btn-primary" onClick={() => onAddToCart(product)}>Add to Cart</button>
          <button
            className={`btn-secondary ${isFavorite ? 'favorite' : ''}`}
            onClick={() => onAddToFavorites(product)}
          >
            {isFavorite ? '★' : '☆'}
          </button>
        </div>
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imageUrl: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    })
  ).isRequired,
  isCartPage: PropTypes.bool,
  isFavoritesPage: PropTypes.bool,
};

export default ProductCard;

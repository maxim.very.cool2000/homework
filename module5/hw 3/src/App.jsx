import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import ProductList from './components/ProductList';
import CartPage from './components/Pages/CartPage';
import FavoritesPage from './components/Pages/FavoritesPage';
import Modal from './components/Modal/Modal';
import ModalHeader from './components/Modal/ModalHeader';
import ModalBody from './components/Modal/ModalBody';
import ModalFooter from './components/Modal/ModalFooter';
import ModalClose from './components/Modal/ModalClose';
import './styles/App.scss';
import cartImage from '../public/icon-header/cart.svg';

const App = () => {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
  const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
  const [isModalOpen, setModalOpen] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  useEffect(() => {
    fetch('/products.json')
      .then(response => response.json())
      .then(data => setProducts(data));
  }, []);

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
  }, [cart]);

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [favorites]);

  const handleAddToCartClick = (product) => {
    setSelectedProduct(product);
    setModalOpen(true);
  };

  const handleAddToCartConfirm = () => {
    setCart([...cart, selectedProduct]);
    setModalOpen(false);
  };

  const handleAddToFavorites = (product) => {
    if (!favorites.includes(product)) {
      setFavorites([...favorites, product]);
    } else {
      setFavorites(favorites.filter(fav => fav.id !== product.id));
    }
  };

  const handleRemoveFromCart = (product) => {
    const index = cart.findIndex(item => item.id === product.id);
    if (index !== -1) {
      const newCart = [...cart];
      newCart.splice(index, 1);
      setCart(newCart);
    }
  };

  const handleRemoveFromFavorites = (product) => {
    setFavorites(favorites.filter(item => item.id !== product.id));
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };

  return (
    <Router>
      <div className="app">
        <header>
          <h1>Euphoria Shop</h1>
          <nav>
            <Link to="/">Home</Link>
            <Link to="/cart"><img src = {cartImage} alt="cartImage" /></Link>
            <Link to="/favorites">Favorite</Link>
          </nav>
        </header>
        <Routes>
          <Route path="/" element={
            <ProductList
              products={products}
              onAddToCart={handleAddToCartClick}
              onAddToFavorites={handleAddToFavorites}
              favorites={favorites}
            />
          } />
          <Route path="/cart" element={
            <CartPage
              cart={cart}
              onRemoveFromCart={handleRemoveFromCart}
              onAddToCart={handleAddToCartClick}
              onAddToFavorites={handleAddToFavorites}
              favorites={favorites}
            />
          } />
          <Route path="/favorites" element={
            <FavoritesPage
              favorites={favorites}
              onRemoveFromFavorites={handleRemoveFromFavorites}
              onAddToCart={handleAddToCartClick}
              onAddToFavorites={handleAddToFavorites}
            />
          } />
        </Routes>
        {isModalOpen && (
          <Modal onClose={handleCloseModal}>
            <ModalHeader>
              <h2>Confirm Add to Cart</h2>
              <ModalClose onClick={handleCloseModal} />
            </ModalHeader>
            <ModalBody>
              <p>Do you want to add {selectedProduct.name} to your cart?</p>
            </ModalBody>
            <ModalFooter
              firstText="Cancel"
              firstClick={handleCloseModal}
              secondaryText="Add to Cart"
              secondaryClick={handleAddToCartConfirm}
            />
          </Modal>
        )}
      </div>
    </Router>
  );
};

export default App;

import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import store from './store';
import App from './App';
import './index.css';
import { ViewModeProvider } from './components/context/ViewModeContext';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <ViewModeProvider>
        <App />
      </ViewModeProvider>
    </Provider>
  </React.StrictMode>,
);

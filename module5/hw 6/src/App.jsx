// src/App.jsx
import React from 'react';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import ProductList from './components/ProductList';
import CartPage from './components/Pages/CartPage';
import FavoritesPage from './components/Pages/FavoritesPage';
import Modal from './components/Modal/Modal';
import ModalHeader from './components/Modal/ModalHeader';
import ModalBody from './components/Modal/ModalBody';
import ModalFooter from './components/Modal/ModalFooter';
import ModalClose from './components/Modal/ModalClose';
import { openModal, closeModal } from './store/modalSlice';
import { addToCart, removeFromCart, removeOneFromCart, addToFavorites, removeFromFavorites, fetchProducts } from './store/productsSlice';
import './styles/App.scss';
import cartImage from "../public/icon-header/cart.svg";

const App = () => {
  const dispatch = useDispatch();
  const isModalOpen = useSelector((state) => state.modal.isOpen);
  const [selectedProduct, setSelectedProduct] = React.useState(null);
  const cart = useSelector((state) => state.products.cart);
  const favorites = useSelector((state) => state.products.favorites);

  React.useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  const handleAddToCartClick = (product) => {
    setSelectedProduct(product);
    dispatch(openModal());
  };

  const handleAddToCartConfirm = () => {
    if (selectedProduct) {
      dispatch(addToCart(selectedProduct));
    }
    dispatch(closeModal());
  };

  const handleCloseModal = () => {
    dispatch(closeModal());
  };

  const handleAddToFavorites = (product) => {
    if (favorites.some(fav => fav.id === product.id)) {
      dispatch(removeFromFavorites(product));
    } else {
      dispatch(addToFavorites(product));
    }
  };

  const handleRemoveFromFavorites = (product) => {
    dispatch(removeFromFavorites(product));
  };

  const handleRemoveFromCart = (product) => {
    dispatch(removeFromCart(product));
  };

  const handleRemoveOneFromCart = (product) => {
    dispatch(removeOneFromCart(product));
  };

  const cartQuantity = cart.reduce((total, product) => total + product.quantity, 0);

  return (
    <Router>
      <div className="app">
        <header>
          <h1>Euphoria Shop</h1>
          <nav>
            <Link to="/">Home</Link>
            <Link to="/cart" className='cart-numbed'>
              <img src={cartImage} alt="cart-icon" className='cart-style' /> {cartQuantity}
            </Link>
            <Link to="/favorites">Favorites</Link>
          </nav>
        </header>
        <Routes>
          <Route path="/" element={
            <ProductList
              onAddToCart={handleAddToCartClick}
              onAddToFavorites={handleAddToFavorites}
              favorites={favorites}
            />
          } />
          <Route path="/cart" element={
            <CartPage
              cart={cart}
              onRemoveOneFromCart={handleRemoveOneFromCart}
              onAddToCart={handleAddToCartClick}
              onAddToFavorites={handleAddToFavorites}
              favorites={favorites}
              onRemoveFromCart={handleRemoveFromCart}
              totalClassName="my-custom-total"
              cartClassName="my-custom-cart-page"
              productCardClassName="product-card-list"
              headerClassName="my-custom-header"
              cartTotalClassName="my-custom-total"
            />
          } />
          <Route path="/favorites" element={
            <FavoritesPage
              favorites={favorites}
              onRemoveFromFavorites={handleRemoveFromFavorites}
              onAddToCart={handleAddToCartClick}
              onAddToFavorites={handleAddToFavorites}
            />
          } />
        </Routes>
        {isModalOpen && selectedProduct && (
          <Modal onClose={handleCloseModal}>
            <ModalHeader>
              <h2>Confirm Add to Cart</h2>
              <ModalClose onClick={handleCloseModal} />
            </ModalHeader>
            <ModalBody>
              <p>Do you want to add {selectedProduct.name} to your cart?</p>
            </ModalBody>
            <ModalFooter
              firstText="Cancel"
              firstClick={handleCloseModal}
              secondaryText="Add to Cart"
              secondaryClick={handleAddToCartConfirm}
            />
          </Modal>
        )}
      </div>
    </Router>
  );
};

export default App;

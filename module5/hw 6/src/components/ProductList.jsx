import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchProducts } from '../store/productsSlice';
import ProductCard from './ProductCard';
import { useViewMode } from './context/ViewModeContext';

const ProductList = ({ onAddToCart, onAddToFavorites, favorites }) => {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.items);
  const productStatus = useSelector((state) => state.products.status);
  const error = useSelector((state) => state.products.error);
  const { viewMode, toggleViewMode } = useViewMode();

  React.useEffect(() => {
    if (productStatus === 'idle') {
      dispatch(fetchProducts());
    }
  }, [productStatus, dispatch]);

  if (productStatus === 'loading') {
    return <div>Loading...</div>;
  }

  if (productStatus === 'failed') {
    return <div>{error}</div>;
  }

  return (
    <div>
      <button onClick={toggleViewMode}>
        {viewMode === 'grid' ? 'Show as Table' : 'Show as Grid'}
      </button>
      <div className={`product-list ${viewMode}`}>
        {products.map((product) => (
          <ProductCard
            key={product.id}
            product={product}
            onAddToCart={onAddToCart}
            onAddToFavorites={onAddToFavorites}
            favorites={favorites}
          />
        ))}
      </div>
    </div>
  );
};

export default ProductList;

import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';

const ModalFooter = ({
  firstText = '',
  secondaryText = '',
  firstClick = null,
  secondaryClick = null,
  firstClass = '',
  secondClass = ''
}) => {
  return (
    <div className="modal-footer">
      {firstText && (
        <Button classNames={firstClass} onClick={firstClick}>
          {firstText}
        </Button>
      )}
      {secondaryText && (
        <Button classNames={secondClass} onClick={secondaryClick}>
          {secondaryText}
        </Button>
      )}
    </div>
  );
};

ModalFooter.propTypes = {
  firstText: PropTypes.string,
  secondaryText: PropTypes.string,
  firstClick: PropTypes.func,
  secondaryClick: PropTypes.func,
  firstClass: PropTypes.string,
  secondClass: PropTypes.string,
};

export default ModalFooter;

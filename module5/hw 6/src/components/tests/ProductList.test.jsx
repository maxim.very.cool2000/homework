import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { ViewModeProvider } from '../context/ViewModeContext';
import store from '../store';
import ProductList from '../components/ProductList';

test('toggles view mode between grid and table', () => {
  render(
    <Provider store={store}>
      <ViewModeProvider>
        <ProductList onAddToCart={() => {}} onAddToFavorites={() => {}} favorites={[]} />
      </ViewModeProvider>
    </Provider>
  );

  const toggleButton = screen.getByText(/Show as Table/i);
  fireEvent.click(toggleButton);

  expect(screen.getByText(/Show as Grid/i)).toBeInTheDocument();
  expect(screen.getByRole('button', { name: /Show as Table/i })).toBeInTheDocument();
});

import { render, screen, fireEvent } from '@testing-library/react';
import FavoritesPage from '../components/Pages/FavoritesPage';

test('renders FavoritesPage and checks modal functionality', () => {
  const favorites = [{
    id: 1,
    name: 'Test Product',
    price: 10.0,
    imageUrl: 'test.jpg',
    color: 'Red',
  }];

  const onAddToCart = jest.fn();
  const onRemoveFromFavorites = jest.fn();

  render(
    <FavoritesPage
      favorites={favorites}
      onAddToCart={onAddToCart}
      onRemoveFromFavorites={onRemoveFromFavorites}
    />
  );

  expect(screen.getByText(/Test Product/i)).toBeInTheDocument();
  expect(screen.getByText(/Price: \$10.00/i)).toBeInTheDocument();

  fireEvent.click(screen.getByText(/Remove from Favorites/i));
  expect(onRemoveFromFavorites).toHaveBeenCalledWith(1);
});

import productsReducer, { fetchProducts, addToCart } from '../store/productsSlice';
import modalReducer from '../store/modalSlice';

test('should handle initial state', () => {
  expect(productsReducer(undefined, { type: 'unknown' })).toEqual({
    items: [],
    status: 'idle',
    error: null,
  });
});

test('should handle fetchProducts', async () => {
  const dispatch = jest.fn();
  const fetchProductsThunk = fetchProducts();
  await fetchProductsThunk(dispatch, () => ({}), {});
  expect(dispatch).toHaveBeenCalled();
});

test('should handle addToCart action', () => {
  const initialState = { cart: [] };
  const product = { id: 1, name: 'Test Product', price: 10.0 };
  const newState = productsReducer(initialState, addToCart(product));
  expect(newState.cart).toContain(product);
});

test('should handle modal state', () => {
  const initialState = { show: false, content: null };
  const newState = modalReducer(initialState, { type: 'SHOW_MODAL', payload: { content: 'Test Content' } });
  expect(newState).toEqual({ show: true, content: 'Test Content' });
});

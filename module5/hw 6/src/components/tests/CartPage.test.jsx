import { render, screen, fireEvent } from '@testing-library/react';
import CartPage from '../components/Pages/CartPage';

test('renders CartPage and checks buttons', () => {
  const cart = [{
    id: 1,
    name: 'Test Product',
    price: 10.0,
    quantity: 1,
  }];

  const onRemoveOneFromCart = jest.fn();
  const onAddToCart = jest.fn();
  const onRemoveFromCart = jest.fn();

  render(
    <CartPage
      cart={cart}
      onRemoveOneFromCart={onRemoveOneFromCart}
      onAddToCart={onAddToCart}
      onRemoveFromCart={onRemoveFromCart}
      onAddToFavorites={() => {}}
    />
  );

  expect(screen.getByText(/Test Product/i)).toBeInTheDocument();
  expect(screen.getByText(/Price: \$10.00/i)).toBeInTheDocument();
  expect(screen.getByText(/Quantity: 1/i)).toBeInTheDocument();

  fireEvent.click(screen.getByText(/Remove/i));
  expect(onRemoveFromCart).toHaveBeenCalledWith(1);

  fireEvent.click(screen.getByText(/Add to Cart/i));
  expect(onAddToCart).toHaveBeenCalledWith({
    id: 1,
    name: 'Test Product',
    price: 10.0,
    quantity: 2,
  });
});

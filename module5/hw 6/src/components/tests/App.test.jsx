import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { ViewModeProvider } from '../context/ViewModeContext';
import store from '../store';
import App from '../App';

test('renders the app and checks for elements', () => {
  render(
    <Provider store={store}>
      <ViewModeProvider>
        <App />
      </ViewModeProvider>
    </Provider>
  );
  
  expect(screen.getByText(/Euphoria Shop/i)).toBeInTheDocument();
  expect(screen.getByText(/Home/i)).toBeInTheDocument();
  expect(screen.getByText(/Cart/i)).toBeInTheDocument();
  expect(screen.getByText(/Favorites/i)).toBeInTheDocument();
});

test('opens and closes the modal', () => {
  render(
    <Provider store={store}>
      <ViewModeProvider>
        <App />
      </ViewModeProvider>
    </Provider>
  );
  
  const addToCartButton = screen.getByText(/Add to Cart/i);
  fireEvent.click(addToCartButton);

  expect(screen.getByText(/Confirm Add to Cart/i)).toBeInTheDocument();
  
  const closeButton = screen.getByRole('button', { name: /×/ });
  fireEvent.click(closeButton);

  expect(screen.queryByText(/Confirm Add to Cart/i)).not.toBeInTheDocument();
});

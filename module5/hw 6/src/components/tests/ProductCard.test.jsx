import { render, screen, fireEvent } from '@testing-library/react';
import ProductCard from '../components/ProductCard';

test('renders ProductCard and buttons', () => {
  const product = {
    id: 1,
    name: 'Test Product',
    price: 10.0,
    imageUrl: 'test.jpg',
    color: 'Red',
  };

  const onAddToCart = jest.fn();
  const onAddToFavorites = jest.fn();

  render(
    <ProductCard
      product={product}
      onAddToCart={onAddToCart}
      onAddToFavorites={onAddToFavorites}
      favorites={[]}
    />
  );

  expect(screen.getByText(/Test Product/i)).toBeInTheDocument();
  expect(screen.getByText(/Price: \$10.00/i)).toBeInTheDocument();
  fireEvent.click(screen.getByText(/Add to Cart/i));
  expect(onAddToCart).toHaveBeenCalledWith(product);

  fireEvent.click(screen.getByText(/☆/i));
  expect(onAddToFavorites).toHaveBeenCalledWith(product);
});

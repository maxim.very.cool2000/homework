import React from 'react';
import { FaStar, FaRegStar } from 'react-icons/fa'; 

const ProductCard = ({ product, onAddToCart, onAddToFavorites, favorites, isCartPage, onRemoveOneFromCart, onRemoveFromCart }) => {
  const isFavorite = favorites.some((fav) => fav.id === product.id);

  return (
    <div className="product-card product-card-page">
      <div className="product-card-content cards">
        <img src={product.imageUrl} alt={product.name} />
        <div className="product-card-details">
          <h3>{product.name}</h3>
          <p>Price: ${product.price.toFixed(2)}</p>
          <button onClick={() => onAddToCart(product)}>Add to Cart</button>
          <button onClick={() => onAddToFavorites(product)}>
            {isFavorite ? <FaStar /> : <FaRegStar />}  
          </button>
          {isCartPage && (
            <div className="cart-product-quantity">
              <button onClick={() => onRemoveOneFromCart(product)}>-</button>
              <span>{product.quantity}</span>
              <button onClick={() => onAddToCart(product)}>+</button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ProductCard;

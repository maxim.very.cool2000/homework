import React, { createContext, useState, useContext } from 'react';

const ViewModeContext = createContext();

export const ViewModeProvider = ({ children }) => {
  const [viewMode, setViewMode] = useState('grid');

  const toggleViewMode = () => {
    setViewMode((prevMode) => (prevMode === 'grid' ? 'table' : 'grid'));
  };

  return (
    <ViewModeContext.Provider value={{ viewMode, toggleViewMode }}>
      {children}
    </ViewModeContext.Provider>
  );
};

export const useViewMode = () => useContext(ViewModeContext);

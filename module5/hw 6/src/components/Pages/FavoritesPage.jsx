import React from 'react';
import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';

const FavoritesPage = ({ favorites, onAddToCart, onAddToFavorites, onRemoveFromFavorites }) => {
  return (
    <div className="favorites-page">
      <h2>Your Favorites</h2>
      <div className='style-favorite'>
        {favorites.map((item) => (
          <ProductCard
            key={item.id}
            product={item}
            onAddToCart={onAddToCart}
            onAddToFavorites={onAddToFavorites} 
            favorites={favorites}
            isFavoritesPage={true} 
          />
        ))}
      </div>
    </div>
  );
};

FavoritesPage.propTypes = {
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      imageUrl: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
    })
  ).isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
  onRemoveFromFavorites: PropTypes.func.isRequired,
};

export default FavoritesPage;

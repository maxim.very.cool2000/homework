import React from 'react';
import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { NumericFormat } from 'react-number-format';
import { useDispatch, useSelector } from 'react-redux';
import { openCheckoutModal, closeCheckoutModal } from '../../store/modalSlice';
import { clearCart } from '../../store/productsSlice';
import Modal from '../Modal/Modal';
import ModalHeader from '../Modal/ModalHeader';
import ModalBody from '../Modal/ModalBody';
import ModalFooter from '../Modal/ModalFooter';
import ModalClose from '../Modal/ModalClose';

const CartPage = ({
  cart,
  onRemoveOneFromCart,
  onAddToCart,
  onAddToFavorites,
  favorites,
  onRemoveFromCart,
  totalClassName = '',
  cartClassName = '',
  productCardClassName = '',
  headerClassName = '',
  cartTotalClassName = '',
}) => {
  const dispatch = useDispatch();
  const isCheckoutModalOpen = useSelector((state) => state.modal.isCheckoutModalOpen);

  if (!cart) return null;

  const total = cart.reduce((acc, item) => acc + item.price * item.quantity, 0);

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required('Required'),
    lastName: Yup.string().required('Required'),
    age: Yup.number().required('Required').min(18, 'Must be 18 or older'),
    address: Yup.string().required('Required'),
    phone: Yup.string().required('Required'),
  });

  const handleCheckout = (values) => {
    console.log('Purchased items:', cart);
    console.log('Customer information:', values);
    console.log('Order successfully placed. Cart is cleared.');
    dispatch(clearCart());
    localStorage.removeItem('cart');
    dispatch(closeCheckoutModal());
  };

  const handleOpenCheckoutModal = () => {
    dispatch(openCheckoutModal());
  };

  const handleCloseCheckoutModal = () => {
    dispatch(closeCheckoutModal());
  };

  return (
    <div className={`cart-page ${cartClassName}`}>
      <header className={headerClassName}>
        <h2>Your Cart</h2>
      </header>
      <ul className="cart-products-list">
        {cart.map((item) => (
          <li key={item.id} className={`cart-product-item ${productCardClassName}`}>
            <ProductCard
              product={item}
              onAddToCart={onAddToCart}
              onAddToFavorites={onAddToFavorites}
              favorites={favorites}
              isCartPage={true}
              onRemoveOneFromCart={() => onRemoveOneFromCart(item)}
              onRemoveFromCart={() => onRemoveFromCart(item)}
            />
          </li>
        ))}
      </ul>
      <div className={`cart-total ${cartTotalClassName}`}>
        <h3>Total: ${total.toFixed(2)}</h3>
      </div>
      <button onClick={handleOpenCheckoutModal}>Checkout</button>
      {isCheckoutModalOpen && (
        <Modal onClose={handleCloseCheckoutModal}>
          <ModalHeader>
            <h2>Checkout</h2>
            <ModalClose onClick={handleCloseCheckoutModal} />
          </ModalHeader>
          <ModalBody>
            <Formik
              initialValues={{
                firstName: '',
                lastName: '',
                age: '',
                address: '',
                phone: '',
              }}
              validationSchema={validationSchema}
              onSubmit={handleCheckout}
            >
              {({ setFieldValue }) => (
                <Form>
                  <div>
                    <label htmlFor="firstName">First Name</label>
                    <Field id="firstName" name="firstName" />
                    <ErrorMessage name="firstName" component="div" />
                  </div>
                  <div>
                    <label htmlFor="lastName">Last Name</label>
                    <Field id="lastName" name="lastName" />
                    <ErrorMessage name="lastName" component="div" />
                  </div>
                  <div>
                    <label htmlFor="age">Age</label>
                    <Field id="age" name="age" type="number" />
                    <ErrorMessage name="age" component="div" />
                  </div>
                  <div>
                    <label htmlFor="address">Address</label>
                    <Field id="address" name="address" />
                    <ErrorMessage name="address" component="div" />
                  </div>
                  <div>
                    <label htmlFor="phone">Phone</label>
                    <NumericFormat
                      id="phone"
                      name="phone"
                      format="(###) ###-##-##"
                      mask="_"
                      customInput={Field}
                      onValueChange={(values) => {
                        setFieldValue('phone', values.value);
                      }}
                    />
                    <ErrorMessage name="phone" component="div" />
                  </div>
                  <button type="submit">Submit</button>
                </Form>
              )}
            </Formik>
          </ModalBody>
          <ModalFooter>
            <button onClick={handleCloseCheckoutModal}>Cancel</button>
          </ModalFooter>
        </Modal>
      )}
    </div>
  );
};

CartPage.propTypes = {
  cart: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      imageUrl: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
      quantity: PropTypes.number.isRequired,
    })
  ).isRequired,
  onRemoveOneFromCart: PropTypes.func.isRequired,
  onAddToCart: PropTypes.func.isRequired,
  onAddToFavorites: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    })
  ).isRequired,
  onRemoveFromCart: PropTypes.func.isRequired,
  totalClassName: PropTypes.string,
  cartClassName: PropTypes.string,
  productCardClassName: PropTypes.string,
  headerClassName: PropTypes.string,
  cartTotalClassName: PropTypes.string,
};

export default CartPage;

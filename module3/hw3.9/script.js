"use strict";

// Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// 4. Яка різниця між nodeList та HTMLCollection?

/*Теоретичні питання 
1. DOM - це дерево елементів в html , яке довзоляє працювати з ними в js , і робити їх більш "гарними".
2. innerText дозволяє замінювати текст в певних тегах , а innerHTML дозволяє створювати тег всередені елементу.
3. Є декілька способів звернення
такі як getElementById, getElementsByClassName, getElementsByTagName, querySelector та querySelectorAll.
На рахунок кращого , для мене дуже зручний getElementsByClassName , або getElementById ,
але відповідно кожен метод звернення є унікальним та відповідає певним задачам які треба зробити
4. яка різниця не дуже розумію(
*/

// Практичні завдання
// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
// Використайте 2 способи для пошуку елементів.
// Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
// 2. Змініть текст усіх елементів h2 на "Awesome feature".
// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

// Практичні завдання
// Завдання 1

let all_colections = document.getElementsByTagName("*");
console.log(all_colections);

let feature_p1 = document.getElementsByClassName("feature");
console.log(feature_p1);

let feature = Array.from(feature_p1);

feature.forEach(function (element) {
  element.style.textAlign = "center";
});
// Завдання 2

let featureTitle = document.getElementsByClassName("feature-title");

let titleh2 = Array.from(featureTitle);

titleh2.forEach(function (element) {
  element.textContent = "Awesome feature";
});

// Завдання 3

let title2 = document.getElementsByClassName("feature-title");

let title_tmw = Array.from(title2);

title_tmw.forEach(function (element) {
  element.textContent = "Awesome feature";
});

let title3 = document.getElementsByClassName("feature-title");

let title3_array = Array.from(title3);

title3_array.forEach(function (element) {
  element.textContent += "!";
});

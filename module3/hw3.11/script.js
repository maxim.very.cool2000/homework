"use strict";

/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

1. Події використовуються для взаємодії з користувачем і створення більш широких можливостей.
2. Є такі події як click (при натискані відбувається дія ,або функція),
   mouseover (при наведенні теж дія ,або функція)
   mouseout (коли мишка покидає об'єкт то відбувається щось)
   mousemove (спрацьовує при русі миші над елементом)
   mousedown та mouseup (спрацьовують при натисканні та відпусканні 
   кнопки миші над елементом відповідно)
   contextmenu (спрацьовує при натисканні правої кнопки миші)

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */
// Завдання 1
let btn_click_id = document.getElementById("btn-click");

let contentNow = document.getElementById("content");
const btnClick = () => {
  let newP = document.createElement("p");
  newP.textContent = "New paragraph";

  contentNow.appendChild(newP);
};

btn_click_id.addEventListener("click", btnClick);

// Завдання 2

let newBtn = document.createElement("button");
content.insertBefore(newBtn, document.getElementById("footer"));
newBtn.id = "btn-input-create";
newBtn.textContent = "Create Input";
const inputbtn = () => {
  let inputbtn = document.createElement("input");
  inputbtn.type = "text";
  inputbtn.placeholder = "Enter text";
  inputbtn.name = "inputName";
  content.insertBefore(inputbtn, newBtn.nextSibling);
};
newBtn.addEventListener("click", inputbtn);

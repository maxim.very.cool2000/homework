/* Теоретичні питання
 1. Як можна оголосити змінну у Javascript?
 2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?
 3. Як перевірити тип даних змінної в JavaScript?
 4. Поясніть чому '1' + 1 = 11.*/
 // 1 через команду var, let або const 
 // 2 string це текстовий рядок який показується в коді текстом , це можуть бути як літери так і символи та цифри.
 // Його можна присвоїти змінній (let string = "your text") також є різновид лапок для нього 'Це рядок одинарними лапками.' `Це рядок з використанням односторонніх лапок.`
 // 3 Перевірити тип данних можна командою typeof (ваша змінна)
 // 4 Тому що '1' це string а 1 це number і джаваскрипт додає текст до цифри
// Практичне завдання (для перевірки розкоментувати після певної умови до наступної)
/*1. Оголосіть змінну і присвойте в неї число.Перевірте, чи ця змінна має тип "number" і виведіть результат в консоль.

let work = 100; 
console.log(typeof work);*/

/*2. Оголосіть змінну name і присвойте їй ваше ім'я. Оголосіть змінну lastName і присвойте в неї Ваше прізвище.
 Виведіть повідомлення у консоль у форматі `Мене звати {ім'я}, {прізвище}` використовуючи ці змінні.

let name = "Maxim";
let lastName = "Shestov";
console.log("My name", name + ",", lastName);*/

/*3. Оголосіть змінну з числовим значенням і виведіть в консоль її значення всередині рядка.
let numberValue = 42;
console.log("Значення змінної: " + numberValue);*/
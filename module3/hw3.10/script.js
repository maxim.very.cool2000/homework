"use strict";

/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */
/* Теоретичні питання
1. Для додавання самі популярні методи це createElement та appenChield
2. parentElem.removeChild(node) - видаляє вузол з parentElem (припускаючи, що node є його дитиною).
3. insertBefore(додає елемент перед елементом)
   insertAfter(додає елемент після елемента)
*/

//  Завдання 1

let teg_a = document.createElement("a");
teg_a.textContent = "Learn More";
teg_a.setAttribute("href", "#");

let footer = document.querySelector("footer");
let p_footer = footer.querySelector("p");
p_footer.insertAdjacentElement("afterend", teg_a);

// Завдання 2 (створення селекту та додавання)

let select = document.createElement("select");
select.setAttribute = ("id", "rating");
let main = document.querySelector("main");
let features = document.querySelector("section");
main.insertBefore(select, features);

// що б було видніше
main.style.display = "flex";
main.style.flexDirection = "column";
main.style.alignItems = "center";
select.style.width = "70px";
select.style.height = "30px";

// Завдання 2 (створення рейтингу)

let optionlist = [
  { value: "4", text: "4 stars" },
  { value: "3", text: "3 stars" },
  { value: "2", text: "2 stars" },
  { value: "1", text: "1 stars" },
];

optionlist.forEach(function (optioninfo) {
  let optionElements = document.createElement("option");
  optionElements.value = optioninfo.value;
  optionElements.textContent = optioninfo.text;
  select.appendChild(optionElements);
});

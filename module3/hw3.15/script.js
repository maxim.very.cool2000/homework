"use strict";
/*Теоретичні питання
1. В чому відмінність між setInterval та setTimeout?
2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?
3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

Практичне завдання:
-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout,
 щоб змінити текстовий вміст елемента div через затримку 3 секунди.
Новий текст повинен вказувати, що операція виконана успішно.

Практичне завдання 2:

Реалізуйте таймер зворотного відліку, використовуючи setInterval. 
При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено". */

// Завдання 1
document.addEventListener("DOMContentLoaded", function () {
  let btn = document.getElementById("btn");
  let txt_p = document.getElementById("textik");
  btn.addEventListener("click", function () {
    setTimeout(function () {
      txt_p.textContent = "Операція виконана успішно";
    }, 3000);
  });
});

// Завдання 2
// document.addEventListener("DOMContentLoaded", function () {
//   let countdown = 10;
//   let timerElement = document.getElementById("timer");

//   let countdownInterval = setInterval(function () {
//     if (countdown > 1) {
//       countdown--;
//       timerElement.textContent = countdown;
//     } else {
//       clearInterval(countdownInterval);
//       timerElement.textContent = "Зворотній відлік завершено";
//     }
//   }, 1000);
// });

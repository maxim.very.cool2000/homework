"use strict";
/*Теоретичні питання
1. В чому полягає відмінність localStorage і sessionStorage?
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
Практичне завдання:
Реалізувати можливість зміни колірної теми користувача.
Технічні вимоги:
- Взяти готове домашнє завдання HW-5 "Book Shop" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. 
При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.
Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.*/

document.addEventListener("DOMContentLoaded", function () {
  const btn = document.querySelector(".collor__btn");
  const blueHeader = document.querySelector("[data-id='blue__header']");
  const whiteHeader = document.querySelector("[data-id='white__header']");
  const cartBtn = document.querySelector("[data-id='cart_button']");
  const footer = document.querySelector("[data-id='footer__container']");
  const main = document.querySelector("[data-id='main__container']");
  const cardColors = document.querySelectorAll("[data-id^='main__item']");

  function toggleClass(element, class1, class2) {
    if (element.classList.contains(class1)) {
      element.classList.remove(class1);
      element.classList.add(class2);
    } else {
      element.classList.remove(class2);
      element.classList.add(class1);
    }
  }

  function saveClassState(element, class1, class2) {
    const currentClass = element.classList.contains(class1) ? class1 : class2;
    localStorage.setItem(element.dataset.id, currentClass);
  }

  function applySavedClassState(element, class1, class2) {
    const savedClass = localStorage.getItem(element.dataset.id);
    if (savedClass) {
      element.classList.remove(class1, class2);
      element.classList.add(savedClass);
    }
  }

  function initializeClassStates() {
    applySavedClassState(blueHeader, "blue__header", "blue");
    applySavedClassState(whiteHeader, "white__header", "white");
    applySavedClassState(cartBtn, "cart_button", "cart__btn");
    applySavedClassState(footer, "footer__container", "footer__new");
    applySavedClassState(main, "main__container", "main__new");

    cardColors.forEach(cardColor => {
      applySavedClassState(cardColor, "main__item", "card__new-color");
    });
  }

  btn.addEventListener("click", function () {
    toggleClass(blueHeader, "blue__header", "blue");
    toggleClass(whiteHeader, "white__header", "white");
    toggleClass(cartBtn, "cart_button", "cart__btn");
    toggleClass(footer, "footer__container", "footer__new");
    toggleClass(main, "main__container", "main__new");

    cardColors.forEach(cardColor => {
      toggleClass(cardColor, "main__item", "card__new-color");
    });

    saveClassState(blueHeader, "blue__header", "blue");
    saveClassState(whiteHeader, "white__header", "white");
    saveClassState(cartBtn, "cart_button", "cart__btn");
    saveClassState(footer, "footer__container", "footer__new");
    saveClassState(main, "main__container", "main__new");

    cardColors.forEach(cardColor => {
      saveClassState(cardColor, "main__item", "card__new-color");
    });
  });
  initializeClassStates();
});

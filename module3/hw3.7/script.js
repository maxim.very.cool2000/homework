"use strict";

/*Теоретичні питання
1. Як можна створити рядок у JavaScript?
2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
3. Як перевірити, чи два рядки рівні між собою?
4. Що повертає Date.now()?
5. Чим відрізняється Date.now() від new Date()?


Практичні завдання
1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
(читається однаково зліва направо і справа наліво), або false в іншому випадку.
2. Створіть функцію, яка перевіряє довжину рядка.
Вона приймає рядок, який потрібно перевірити,
максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині,
 і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false
3. Створіть функцію, яка визначає скільки повних років користувачу.
 Отримайте дату народження користувача через prompt. Функція повина повертати значення повних років на дату виклику функцію.*/
/*Теоретичні питання
1. За допомогою змінної let text = "тут текст" , або за допомогою new String (let text = new String ("тут текст");)


2. Між одинарними та подвійними різниці немає це одне й те саме , але зворотні дозволяють в собі розміщувати більш широкий спектр виводу ,
 let exempple = "гарний текст";
 console.log (`Ви бачите що це ${exemple}`) і вивід Ви бачите що це гарний текст .


 3. В js усе можна порівнювати рядки такою дією == , === .


 4. Повертає кількість мілісекунд, що пройшли з 1 січня 1970 року 00:00:00 UTC (відомого як "Епоха") до моменту виклику методу.

 5. Date.now() робить те що я написав в попередньому завданні ,
 а new Date() ствроює об'єкт Date та отримує дату на той момент яка дата зараз.

 */
// Практичні завдання
// Завдання 1

// let palindrome = prompt("Введіть слово паліндром:");

// function isPalindrome(inputString) {
//   let result = inputString === inputString.split("").reverse().join("");
//   return result;
// }

// let isPalindromResult = isPalindrome(palindrome);
// console.log(isPalindromResult);

// Завдання 2

// function checking(userString, lengthValue) {
//     let lengthString = userString.length;
//     console.log(lengthString);
//     if (lengthString <= lengthValue) {
//       return true;
//     } else {
//       return false;
//     }
//   }

//   let userString = prompt("Введіть рядок:");
//   let lengthValue = prompt("Введіть значення скільки тут має бути символів включно з пробілами:");

//   let isLengthValid = checking(userString, lengthValue);
//   console.log(isLengthValid);

// Завдання 3

// function calculateAge(birthYear, birthMonth, birthDay) {
//     if (isNaN(birthYear) || isNaN(birthMonth) || isNaN(birthDay)) {
//         throw new Error('Введені значення не є числами.');
//     }


//     if (birthMonth < 1 || birthMonth > 12) {
//         throw new Error('Некоректний місяць.');
//     }


//     if (birthDay < 1 || birthDay > 31) {
//         throw new Error('Некоректний день.');
//     }

//     let currentDate = new Date();
//     let birthDate = new Date(birthYear, birthMonth - 1, birthDay);
//     let age = currentDate.getFullYear() - birthDate.getFullYear();

//     if (currentDate.getMonth() < birthDate.getMonth() ||
//         (currentDate.getMonth() === birthDate.getMonth() && currentDate.getDate() < birthDate.getDate())) {
//         age--;
//     }

//     return age;
// }

// let birthYear = prompt("Введіть рік народження");
// let birthMonth = prompt("Введіть місяць народження (число від 1 до 12)");
// let birthDay = prompt("Введіть день народження (число від 1 до 31)");

// try {
//     birthYear = parseInt(birthYear);
//     birthMonth = parseInt(birthMonth);
//     birthDay = parseInt(birthDay);

//     let userAge = calculateAge(birthYear, birthMonth, birthDay);
//     console.log("Повних років: " + userAge);
// } catch (error) {
//     console.error(error.message);
// }


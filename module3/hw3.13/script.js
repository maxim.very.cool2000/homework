/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
2. В чому сенс прийому делегування подій?
3. Які ви знаєте основні події документу та вікна браузера? 

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

document.addEventListener("DOMContentLoaded", function () {
  let tabsList = document.querySelector(".centered-content");
  tabsList.addEventListener("click", function (event) {
    if (event.target.classList.contains("tabs-title")) {
      let tabsItem = tabsList.querySelectorAll(".tabs-title");
      tabsItem.forEach(function (tab) {
        tab.classList.remove("active");
      });
      event.target.classList.add("active");
      let tabText = event.target.innerText.toLowerCase().trim();
      let tabContent = tabsList.querySelectorAll(".tabs-content li");
      tabContent.forEach(function (content) {
        content.style.display = "none";
      });
      let activeContent = tabsList.querySelector(`.tabs-content li.${tabText}`);
      if (activeContent) {
        activeContent.style.display = "block";
      }
    }
  });
});

// // ## Завдання 1

// // Напишіть функцію `mergeArrays` для об'єднання кількох масивів на один.

// // Функція має необмежену кількість параметрів.
// // Функція повертає один масив, який є збірним із масивів,
// // переданих функції як аргументи під час її виклику.

// // Умови:

// // - Усі аргументи функції повинні мати тип «масив», інакше генерувати
// //   помилку;
// // - У помилці обов'язково вказати, який за рахунком аргумент провокує помилку.

// // const arrey1 = [1, 2, 3, "abc"];
// // const arrey2 = [4, 5, 6, null, true];
// // function mergeArrays() {
// //   for (let i = 0; i < arguments.length; i++) {
// //     if (!Array.isArray(arguments[i])) {
// //       alert("error");
// //       return;
// //     }
// //   }
// //   let result = [];
// //   for (let i = 0; i < arguments.length; i++) {
// //     result = result.concat(arguments[i]);
// //   }
// //   return result;
// // }

// // console.log(mergeArrays(arrey1, arrey2));

// function developerSkillInspector() {
//   const skillsList = [];

//   let skillName;
//   let skillLevel;

//   let skillID = 0;

//   do {
//     skillName = prompt("Enter your skill:");
//     skillLevel = prompt("Enter your level (USE ONLY junior, middle, senior!):");

//     if (
//       skillName &&
//       skillName.trim().length > 1 &&
//       (skillLevel === "junior" ||
//         skillLevel === "middle" ||
//         skillLevel === "senior")
//     ) {
//       skillsList.push({ id: skillID++, skillName, skillLevel });
//     }
//   } while ((skillsList.length < 1, skillName !== null, skillLevel !== null));

//   console.log(skillsList);
// }

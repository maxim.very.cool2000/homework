"use strict";
// Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Завдання
// Дано масив books.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

function validateBook(book) {
  const requiredFields = ["author", "name", "price"];

  try {
    requiredFields.forEach((field) => {
      if (!(field in book)) {
        throw new Error(
          `Invalid book object: ${JSON.stringify(
            book
          )}. Missing property: ${field}`
        );
      }
    });
    return true;
  } catch (error) {
    console.error(error.message);
    return false;
  }
}

function displayBooks(books) {
  const root = document.getElementById("root");
  const ul = document.createElement("ul");

  books.forEach((book) => {
    if (validateBook(book)) {
      const li = document.createElement("li");
      li.textContent = `Назва книги: ${book.name}, автор книги: ${book.author}, ціна: ${book.price}`;
      ul.appendChild(li);
    }
  });

  root.appendChild(ul);
}

displayBooks(books);

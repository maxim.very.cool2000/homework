"use strict";
// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript 

// На мою думку асинхронні запити в Javascript 
// Допомагають виконувати шматки коду так що, поки виконується один шматок, 
// може початись інший , не чекаючи поки закінчиться попередній і все буде працювати добре


// Практика 
document.getElementById('find-ip-btn').addEventListener('click', async () => {
    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const ipData = await ipResponse.json();
        const ipAddress = ipData.ip;

        const locationResponse = await fetch(`http://ip-api.com/json/${ipAddress}?fields=continent,country,regionName,city,district`);
        const locationData = await locationResponse.json();
        console.log(locationData);
        const infoDiv = document.getElementById('info');
        infoDiv.innerHTML = `
          <p><strong>Континент:</strong> ${locationData.continent || 'Невідомо'}</p>
          <p><strong>Країна:</strong> ${locationData.country || 'Невідомо'}</p>
          <p><strong>Регіон:</strong> ${locationData.regionName || 'Невідомо'}</p>
          <p><strong>Місто:</strong> ${locationData.city || 'Невідомо'}</p> 
          <p><strong>Район:</strong> ${locationData.district || 'Невідомо'}</p>

        `;
    } catch (error) {
        console.error('Error fetching data:', error);
    }
});

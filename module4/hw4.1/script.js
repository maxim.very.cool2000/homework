"use strict";
// Теоретичне питання
//1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Практичне завдання
//1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
//2. Створіть гетери та сеттери для цих властивостей.
//3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
//4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
//5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// Теоретичне
// 1.Прототипне наслідування в JavaScript означає,
// що кожен об'єкт може мати посилання на інший об'єкт, який є його прототипом. Коли властивість або метод не знаходиться в самому об'єкті,
// JavaScript буде шукати його в його прототипі,
// а так далі у ланцюжку прототипів, поки не буде знайдено потрібний елемент або не буде досягнуто кінця ланцюжка прототипів.

// Практичне
class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }

  set age(value) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const programmer1 = new Programmer("Martin Lourence", 30, 3000, [
  "JavaScript",
  "Python",
]);
const programmer2 = new Programmer("Garik Karagodskyi", 25, 2000, [
  "Java",
  "C++",
]);

console.log("Programmer 1:", programmer1);
console.log("Programmer 2:", programmer2);

// <!-- ##TASK 1

// Є сервер https://ajax.test-danit.com/api-pages/swapi.html

// Створити клас Films, який надсилає AJAX запит на адресу
// `https://ajax.test-danit.com/api/swapi/films/`, отримує список усіх фільмів та
// виводить назви на сторінку в root.

// ##TASK 2

// Отримати всі планети з кіносвіту Star Wars
// https://ajax.test-danit.com/api-pages/swapi.html

// Вивести планети списком у порядку зменшення радіусу планети. Реалізувати через
// клас.

// ##TASK 3

// Отримати всі фільми з кіносвіту Star Wars
// https://ajax.test-danit.com/api-pages/swapi.html

// Вивести списком назви фільмів та планети, що належать до фільму.
// реалізувати через клас. -->

const BASE_URL = "https://ajax.test-danit.com/api/swapi/films/";
let root = document.querySelector("#root");

class Films {
  getFilms(url) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.open("GET", `${url}`);
      xhr.send();

      xhr.onload = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
          resolve(JSON.parse(xhr.response));
        } else {
          reject(new Error("data error"));
        }
      };

      xhr.onerror = (e) => {
        reject(e);
      };
    });
  }
  createList(data) {
    let list = document.createElement("ul");
    const films = data.map(({ name }) => {
      let li = document.createElement("li");
      li.append(name);
      return li;
    });
    list.append(...films);
    return list;
  }
}

const film = new Films();

film
  .getFilms(BASE_URL)
  .then((data) => {
    root.append(film.createList(data));
    console.log(data);
  })
  .catch((e) => {
    console.log(e);
  });

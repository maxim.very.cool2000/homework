"use strict";

// const baz = { a: 3 };

// function foo() {}

// foo.prototype = baz;

// const bar = Object.create(baz);

// console.log(bar.__proto__ === foo.prototype);
// console.log(bar instanceof foo);

// ! CLASS

// class User {
// 	#id;
// 	#age;

// 	static #count = 0;
// 	static instances = [];

// 	constructor(name, age) {
// 		this.name = name;
// 		this.age = age;
// 		this.#id = Math.round(Math.random() * 10);

// 		User.#count++;
// 		User.instances.push(this);
// 	}

// 	set age(value) {
// 		console.log("setter", this);
// 		this.#age = value;
// 	}

// 	get age() {
// 		console.log("getter", this);
// 		return this.#age;
// 	}

// 	static getCount = () => User.#count;

// 	getAge() {
// 		return this.age;
// 	}

// 	#getID() {
// 		return this.#id;
// 	}

// 	static getUserFullInfo = (obj) =>
// 		`User ID${obj.#id}, ${obj.name}, ${obj.age} years`;
// }

// console.log(typeof User);

// const user = new User("Uasya", 18);
// const user2 = new User("Uasilisa", 18);

// console.log(user);
// console.log(user2);
// console.log(user.getAge());
// ! console.log(user.#id);

// * Object.keys(), Object.assign({}, {})
// * Array.prototype.foreach

// console.log(User.getUserFullInfo(user));

// console.log(User.getCount());

// const user3 = {};

// console.log(user instanceof User);
// console.log(user2 instanceof User);
// console.log(user3 instanceof User);

// console.log(User.instances);

// class Guest extends User {
// 	constructor(name, age) {
// 		super(name, age);

// 		this.lastVisit = new Date();
// 	}

// 	getAge() {
// 		// const parentAgeValue = super.getAge();
// 		const parentAgeValue = 10;
// 		return `Age this guest maybe equal ${this.age}`;
// 	}
// }

// const guest = new Guest("Anonimous", 10);
// console.log(guest);
// console.log(user);
// console.log(guest.getAge());
// console.log(user.getAge());
/*##TASK 1

Напишіть клас Пацієнт із такими параметрами:

- ПІБ;
- дата народження;
- стать;

І на його основі створіть 2 розширені класи:

1. Пацієнт Кардіолога, з додатковими параметрами:

- Середній тиск;
- перенесені проблеми із серцево-судинною системою;

2. Пацієнт Стоматолога з додатковими параметрами:

- Дата останнього візиту;
- поточне лікування; */
// class Patient {
//   constructor(fullName, birthDate, sex) {
//     this.fullName = fullName;
//     this.birthDate = birthDate;
//     this.sex = sex;
//   }
// }

// class CardiologistPatient extends Patient {
//   constructor(fullName, birthDate, sex, pressure, problems) {
//     super(fullName, birthDate, sex);
//     this.pressure = pressure;
//     this.problems = problems;
//   }
// }

// const cardiologistPatient = new CardiologistPatient(
//   "Mike Smith",
//   "12.12.1989",
//   "male",
//   "120/80",
//   "no"
// );
// console.log(cardiologistPatient);

// class DentistPatient extends Patient {
//   constructor(fullName, birthDate, sex, lastVisit, prescription) {
//     super(fullName, birthDate, sex);
//     this.lastVisit = lastVisit;
//     this.prescription = prescription;
//   }
// }
// - id спливаючого вікна;
// - класи спливаючого вікна;
// - Текст всередині тега p;

// const root = document.querySelector("#root");

// class Modal {
//   #modal;
//   constructor(id, classes, innerText) {
//     this.id = id;
//     this.classes = classes;
//     this.innerText = innerText;
//   }

//   render() {
//     this.#modal = document.createElement("div");
//     this.#modal.id = this.id;
//     this.#modal.classList.add("modal", ...this.classes);

//     const modalContent = document.createElement("div");
//     modalContent.classList.add("modal-content");

//     const span = document.createElement("span");
//     span.classList.add("close");
//     span.innerHTML = "&times;";

//     span.addEventListener("click", this.closeModal.bind(this));

//     const modalText = document.createElement("p");
//     modalText.innerText = this.innerText;

//     modalContent.append(span, modalText);
//     this.#modal.append(modalContent);

//     return this.#modal;
//   }

//   openModal() {
//     this.#modal.classList.add("active");
//   }

//   closeModal() {
//     this.#modal.classList.remove("active");
//   }
// }

// const modal = new Modal(
//   "02",
//   ["some-class", "another-class"],
//   "some inner text"
// );
// root.append(modal.render());
// const openModalButton = document.querySelector("#myBtn");
// openModalButton.addEventListener("click", modal.openModal.bind(modal));

let data

function loaddata() {
  setTimeout(() => { data = 1; }, 5000)
}
let promise = new Promise(loaddata)
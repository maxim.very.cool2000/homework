##TASK 1

Напишіть клас Пацієнт із такими параметрами:

- ПІБ;
- дата народження;
- стать;

І на його основі створіть 2 розширені класи:

1. Пацієнт Кардіолога, з додатковими параметрами:

- Середній тиск;
- перенесені проблеми із серцево-судинною системою;

2. Пацієнт Стоматолога з додатковими параметрами:

- Дата останнього візиту;
- поточне лікування;

##TASK 2

Завдання: напишіть клас Modal, який створюватиме об'єкт, що описує
спливаюче вікно. Параметри об'єкта:

- id спливаючого вікна;
- класи спливаючого вікна;
- Текст всередині тега p;

Об'єкт має мати 3 методи:

- render, який повертає DOM-елемент спливаючого вікна з такою розміткою:

        <div id="idВікна" class="modal іншіКласиВікна">
            <div class="modal-content">
                <span class="close">&times;</span>
                <p>Текст вікна</p>
            </div>
        </div>

- openModal, який відкриває вікно (його потрібно використовувати як обробник події click для button з id="myBtn"). Вікно відкривається (показується користувачеві) при додаванні до елементу з класом `modal` класу `active`.

- closeModal - який закриває вікно при натисканні на хрестик (span з класом `close`) усередині вікна.

        <style>
        body {
        font-family: Arial, Helvetica, sans-serif;
        }

        /* The Modal (background) */
        .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
        }

        .modal.active {
        display: block;
        }

        /* Modal Content */
        .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
        }

        /* The Close Button */
        .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
        }

        .close:hover,
        .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
        }

        </style>

        <div id="root"></div>
        <button id="myBtn">Open Modal</button>

##TASK 3

Візьміть код з попередньої задачі і створіть один універсальний клас `Modal`, який
відповідає за каркас спливаючого вікна, і на його основі 2 інших:

1.  Модальне вікно з формою реєстрації `ModalRegister`. Його HTML-розмітка виглядатиме так:

        <div id="idОкна" class="классыОкна">
        <div class="modal-content">
        <span class="close">&times;</span>
            <form action="" id="register-form">
                <input type="text" name="login" placeholder="Ваш логін" required>
                <input type="email" name="email" placeholder="Ваш email" required>
                <input type="password" name="password" placeholder="Ваш пароль" required>
                <input type="password" name="repeat-password" placeholder="Повторіть пароль" required>
                <input type="submit" value="Реєстрація">
            </form>
        </div>
        </div>

2.  Модальне вікно з формою авторизації `ModalAuth`. Його HTML-розмітка:

        <div id="idОкна" class="классыОкна">
        <div class="modal-content">
        <span class="close">&times;</span>
            <form action="" id="login-form">
                <input type="text" name="login" placeholder="Ваш логін або email" required>
                <input type="password" name="password" placeholder="Ваш пароль" required>
                <input type="submit" value="Вхід">
            </form>
        </div>
        </div>

Прив'яжіть відкриття першого вікна до кнопки Реєстрація, а другого до кнопки Вхід. Кнопки можна створити одразу в HTML

##TASK 4

Доповніть код попередньої задачі.

Напишіть універсальний клас `Input`, який створюватиме однорядкове поле
введення. У нього будуть такі параметри:

- тип (text, email, password, number, date, submit тощо);
- name;
- обов'язкове поле чи ні;
- id;
- Класи;
- placeholder;
- errorText
- текст помилки, якщо поле обов'язково до заповнення та не було заповнено. У разі ппомилки виводити під полем текст помилки червоного кольору.

А також такі методи як:

- render – повертає HTML-розмітку форми;
- handleBlur - перевіряє значення в полі при втраті фокусу, якщо поле обов'язково до заповнення.

Також напишіть клас `Form`, який створюватиме HTML-форму, і у якого
будуть такі параметри:

- id;
- Класи;
- action;

А також чотири методи:

- render – який створює каркас форми та повертає його;
- handleSumbit - який відповідає за обробку відправки форми;
- serialize, який всі заповнені поля форми збирає в рядок формату: "nameПоля=значенняПоля";
- serializeJSON, який всі заповнені поля форми збирає в об'єкт, ключами
  якого будуть значення name

##TASK 5

Візьміть код із попереднього завдання, і створіть два дочірні класи `RegisterForm` та
`LoginForm` для класу `Form`.

1. RegisterForm:

- метод render, який буде повертати форму з 4 полями введення: Логін, email, Пароль та
  Повторіть пароль. Для відображення полів використовуйте клас Input із попереднього
  завдання.
- метод handleSumbit, що додатково перевірятиме, чи збігаються паролі з
  полів Пароль та ПовторітьПароль.

2. LoginForm:

- метод render, який буде повертатиме форму з 2 полями введення: Логін/email та Пароль.
  Для відображення полів використовуйте клас Input із попереднього завдання.
- метод handleSumbit, що перевірятиме чи заповнені обидва поля введення.

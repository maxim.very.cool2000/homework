// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій.
// Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts

// 1 Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// 2 Кожна публікація має бути відображена у вигляді картки (приклад доданий в репозиторії групи, посилання на ДЗ вище),
// та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// 3 На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
// При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
// Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// 4 Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// 5 Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені.
// Це нормально, все так і має працювати.
// 6 Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card.
//  При необхідності ви можете додавати також інші класи.

class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
    this.element = this.createCardElement();
  }

  createCardElement() {
    const card = document.createElement('div');
    card.className = 'card';

    const title = document.createElement('h2');
    title.textContent = this.post.title;
    card.appendChild(title);

    const body = document.createElement('p');
    body.textContent = this.post.body;
    card.appendChild(body);

    const userInfo = document.createElement('p');
    userInfo.textContent = `Posted by: ${this.user.name} (${this.user.email})`;
    card.appendChild(userInfo);

    const deleteButton = document.createElement('button');
    deleteButton.textContent = 'Delete';
    deleteButton.addEventListener('click', () => this.deleteCard());
    card.appendChild(deleteButton);

    return card;
  }

  deleteCard() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
      method: 'DELETE'
    })
      .then(response => {
        if (response.ok) {
          this.element.remove();
        } else {
          console.error('Failed to delete the post');
        }
      })
      .catch(error => console.error('Error:', error));
  }
}
document.addEventListener("DOMContentLoaded", () => {
  const container = document.getElementById('post-container');

  fetch("https://ajax.test-danit.com/api/json/users")
    .then(res => res.json())
    .then(users => {
      fetch("https://ajax.test-danit.com/api/json/posts")
        .then(res => res.json())
        .then(posts => {
          posts.forEach(post => {
            const user = users.find(user => user.id === post.userId);
            const card = new Card(post, user);
            container.appendChild(card.element);
          });
        });
    })
    .catch(error => {
      console.error("Error:", error);
    });
});

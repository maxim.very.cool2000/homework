"use strict";

let http = "https://ajax.test-danit.com/api/swapi/films";
const div = document.createElement("div");
document.body.append(div);
const ul = document.createElement("ul");
div.appendChild(ul);

fetch(http)
  .then((films) => films.json())
  .then((data) => {
    console.log(data);
    data.forEach((film) => {
      const filmLi = document.createElement("li");
      ul.appendChild(filmLi);
      filmLi.innerHTML = `<strong>Episode ${film.episodeId}: ${film.name}</strong><br>${film.openingCrawl}<br><br>`;

      const charactersUl = document.createElement("ul");
      filmLi.appendChild(charactersUl);

      film.characters.forEach((characterURL) => {
        fetch(characterURL)
          .then((response) => response.json())
          .then((character) => {
            const characterLi = document.createElement("li");
            charactersUl.appendChild(characterLi);
            characterLi.textContent = `${character.name}`;
            console.log(character);
          })
          .catch((error) =>
            console.error("Помилка при отриманні даних про персонажа:", error)
          );
      });
    });
  })
  .catch((error) =>
    console.error("Помилка при отриманні даних про фільми:", error)
  );

console.log(http);
